demoApp.controller('TranslateController2', ['translation','$location', '$rootScope', '$scope', '$http', function (translation,$location, $rootScope, $scope, $http) {	
	
	//TODO: refactor those static data
	
	popovers2();
	
	$scope.handlePasted = function($event) {
		//console.log($event);		
		var elem = $('#input-box')[0];
		//console.log(elem+','+$rootScope);
		handlepaste(elem, $event.originalEvent, $scope,$http,$rootScope);
	}
	//
	//TODO: the method has been create to avoid
	//translate method invoked twice (by $routeProvide and header.html binding)
	$scope.initialize = function(){
		//console.log('initialize in TranslateController');
		$scope.text = 'Enter text to translate in any language';		
		//TODO: replace hardcoded english to the detection
		$rootScope.autodetection = true;
		$rootScope.showSrclangDropdown = true;
		if ($rootScope.srcLang==null) {
			$rootScope.srcLang = 'English';
		}
		if ($rootScope.destLang==null) {
			$rootScope.destLang = 'French';
		}
		$rootScope.message='Language of the source text is ';
		//translate($scope.destLang,$scope.srcLang,$scope,$rootScope);
		$http.get('/frengly/static/userlangs.json').success(function(data, status, headers, config) {$rootScope.userLangs = data.list;}).error(function(data, status, headers, config) {});				
		//prepareInputBoxes();	
	}
	
	
	$scope.translate = function() {	
		console.log('inside translate...');
		
		
		var textInput = $scope.text;
		var delimiter = '\n';	
		try{
			textInput = textInput.replace(new RegExp('<div>', 'g'),delimiter).replace(new RegExp('</div>', 'g'),'').replace(new RegExp('<br>', 'g'),'').replace(new RegExp('&nbsp;', 'g'),' ');
		} catch(ex){console.log(ex);}
		

		//DATA
        var data = {
    		 text : textInput, 
    		 srcLang: $rootScope.srcLang, 
    		 destLang: $rootScope.destLang
        }        
        //SUCCESS
        var success = function(data){
        	 $scope.translationChunks = data.list;
	         console.log('translation OK.');     
	    }	    
        //ERROR
	    var error = function(data){    	
	    	console.log('translation ERROR.');  	  
	    }
      
	    
	    translation.translate(data).success(success).error(error);
		
		//$('#display-box').html('<img src="images/throbber-mini.gif" />');
		
		//log.info(rootURL);
		/*
		$.ajax({
			//url: callURL+'controller?action=translateAnyAny&rest=1&s_langs='+srcLang+'&lb_langs='+destLangISO,
			url: callURL,
			dataType: 'json',
			contentType: 'application/json;charset=utf-8',
			data: JSON.stringify({text : textInput, srcLang: srcLang, destLang: destLangISO}),			    
		    method: 'post',
		    success: function(data){
				
				var htmlDest = '';
				
				$('#display-box').empty();
				$('#input-box-over-in').empty();
				
				$.each(data.list, function(i, row) {				
					//copy_pair(row.srcWord,row.destWord);
				
						
							var uniqueId = row.uniqueId;	
							var src = row.srcWord;
							src = src.replace(new RegExp(delimiter, 'g'),'<br>');
						    var dest = row.destWord;
						    dest = dest.replace(new RegExp(delimiter, 'g'),'<br>');
						    
						    var enId = row.srcId;
						    var pid = row.destId;
						    if (srcLang=='en' || srcLang=='English') {
						    	enId = row.srcId;
						    	pid = row.destId;
						    } else {
						    	enId = row.destId;
						    	pid = row.srcId;
						    }
						   			    
							$('#input-box-over-in').append('<span id="left__'+uniqueId+'" onmouseover="addClass(this)" onmouseout="removeClass(this)">'+src+'</span>');
							$('#display-box').append('<span class="hoverable" id="right__'+uniqueId+'__'+enId+'" onmouseover="addClass(this)" onmouseout="removeClass(this)" data-idem="'+uniqueId+'" data-poload="true" data-toggle="jtol-marker">'+dest+'</span>');
							
							$($('#display-box').children().last()).bind('click',function() {
								//hide
							    var e=$(this);
							    e.unbind('click');
							    loadPopoverData(e,srcLang,destLangISO,enId,src,pid,row,$rootScope);
							    
							    
							});


							
							$('#display-box').children().last().hover(function(){					
								scrollTopAsBottom();
							});
					

					
		    	});
				$('#input-box').html(data.text);
				
				//!!! for multilang translation always put in the input box oryginal text !!!
				if (destLangISO!='English' && srcLang!='English'){
					$('#input-box-over-in').html(data.text);
				}
				
				//boxes();
				
				
				

				
			},
			error: function(errormessage){
				$('#display-box').empty();
				toastr.error('Error:'+errormessage);
			}
		});*/
	}
	
	$scope.addButton = function(key,value) {
		var langs = $rootScope.userLangs;
		langs[key] = value; //add a new btn here
		//console.log('addButton');
	}
	
	$scope.changeLanguage = function(language) {
		$rootScope.destLang = language;
		$scope.translate();
		//translate(language,$rootScope.srcLang,$scope,$rootScope);
	}
	
	//could be anywhere, but since it is related to the translate page...
	$scope.$on('$locationChangeStart', function (event, next, current) {        
        if (current.indexOf('translate')>-1 || current.indexOf('contribute')>-1){
        	//console.log('leaving translate page');
        	$rootScope.showSrclangDropdown = false;
        	$rootScope.message = '';
        } 
    });
	
	$scope.manualSrcLang = function(language) {
		//toastr.info('Autodetection is off'); but only once
		if (language=='autodetection') {
			$rootScope.autodetection = true;
			detect2($scope,$http,$rootScope);
			return;
		}
		$rootScope.autodetection = false;
		//console.log('setting srcLang to:'+language);
		$rootScope.srcLang = language;
	}
	

	
	$scope.changeSrcText = function(e) {		

		
		//if you start typing - clear the translation box
		//if (!$('#display-box').is(':empty')) 
		$('#display-box').empty();
		
		//change behaviour of mouseover
		$('#display-box').unbind('mouseover');
				
		
		
		//$rootScope.message='';
		if (e.keyCode == '13') {
			e.preventDefault();
			//console.log('changeSrcText, enter clicked');			
			translate($scope.destLang,$scope.srcLang,$scope,$rootScope);
            return;
        }	
		//hideOverlay=1;
		detect2($scope,$http,$rootScope);
	}
	

	
}]);



//var screen = $("#home-screen");
var map = new Object();
var mapReversed = new Object();


//Upon mouseover, show the overlay above the input box - thats why Cursor dissapears!!!!!!!!!!!!!!!!!!!!
//This function is very important to display yellow markers in the top overlayer on top of input area
function boxes(){
	
	
	
	$('#display-box').mouseover(function(){
		/*
		var isNewPage = $('#input-box-over-in').html().length==0;
		var isWriting = (hideOverlay==1);			
		if (isWriting==false && isNewPage==false) {
			var pos = $('#input-box').scrollTop();
			$('#input-box-over').show();					
			$('#input-box-over-in').scrollTop(pos);
		} */
		$('#input-box-over').show();
		
		//console.log('show input-box-over');
	}).mouseleave(function(){
		$('#input-box-over').hide();
		//console.log('hide input-box-over');
	});
}

/*function onEnter(){
	
	$("document").keypress(function(e){
		alert('enter on input box');
		if (e.keyCode == '13') {
			alert('enter on input box');
        }
	});
	
}*/

function popovers2($rootScope){
	$('html').on('click', function(e) {
		  $('.popover').each( function() {
		    if( $(e.target).get(0) !== $(this).prev().get(0)  && $('.popover').has(e.target).length === 0) {
		      $(this).popover('hide');
		      //translate		      
		      /*var srcLang = $(this).find(".popover-content").find('.popoverContainer').data('srclang');
		      var destLang = $(this).find(".popover-content").find('.popoverContainer').data('destlang');
		      translate(srcLang,destLang);*/
		    } else {
		    	var srcLang = $(this).find(".popover-content").find('.popoverContainer').data('srclang');
			    var destLang = $(this).find(".popover-content").find('.popoverContainer').data('destlang');
			    
			    if (srcLang!=null && destLang!=null){
				    var popoverId = $(this).find(".popover-content").find('.popoverContainer').data('id');
				    var enId = $(this).find(".popover-content").find('.popoverContainer').data('enid');
				    var srcWord = $(this).find(".popover-content").find('.popoverContainer').data('srcword');
				    var pid = $(this).find(".popover-content").find('.popoverContainer').data('pid');				    
				    //console.log('updating popovers foo:'+srcLang+','+destLang);				    
			    	updatePopover(popoverId,srcLang,destLang,enId,srcWord,pid,$rootScope);
		    	}
		    }
		  });
	});
}




//we use bootstrap popover
function loadPopoverData(e,srcLang,destLangISO,enId,src,pid,row,$rootScope){
    var url = '/frengly/data/getByEnId?srcLang='+srcLang+'&destLang='+destLangISO+'&enId='+enId+'&srcWord='+src;
    var popoverId = new Date().getTime();
    
    $.ajax({
    		url: url,    		
    		contentType: 'application/json;charset=utf-8',						    		
    		success: function(d) {
    				//console.log('success111'+srcLang);
			    	var html = '';
			    	$.each(d.list, function(i, row) {
		        		var pid = row.pid;
		        		
		        		//TODO: display also priority (cases or aspect) and update it on setPriority
		        		var delIcon = '<span><img src="images/trash.png" class="centered-image" style="cursor:pointer;" onclick="deleteByPid(\''+srcLang+'\',\''+destLangISO+'\','+pid+',\''+row.srcWord+'\','+row.en_id+','+popoverId+')" /></span>';					        		
		        		
		        		html=html+delIcon+'&nbsp;<span onclick="setPriority(\''+srcLang+'\',\''+destLangISO+'\','+pid+',\''+row.srcWord+'\','+row.en_id+','+popoverId+')" data-enId="'+row.en_id+'" id="'+ pid +'">'+row.destWord+'&nbsp; <span data-priority="'+pid+'" style="cursor: pointer; font-weight: bold">'+row.priority+'</span></span><br/>';
						//var priorLink = '<a onclick="setPriority('+digit+','+alignmentObjId+',\''+pidRow+'\',\''+enId+'\',\''+srcLang+'\',\''+destLang+'\',\''+srcWord+'\')">'+word+aspect+'</a>';					
		        	});
			    	
			    	
			    	html=html+'<input onkeydown="addPair(event,\''+srcLang+'\',\''+destLangISO+'\','+pid+',\''+row.srcWord+'\','+row.en_id+','+popoverId+')" class="form-control" data-srcWord="'+src+'" data-srcLang="'+srcLang+'" data-destLang="'+destLangISO+'" type="text" />';
			    	
			    			    	
			    	html='<div class="popoverContainer" data-enId="'+enId+'" data-srcWord="'+src+'" data-pid="'+pid+'" data-srcLang="'+srcLang+'" data-destLang="'+destLangISO+'" id="'+popoverId+'">'+html+'</div>';
			    	e.popover({html: true,placement: 'bottom',content: html}).popover('show');
			        
    		},
    		error:  function(){
    			toastr.info('Phrase details available only for pairs with English language');
    		}
    });
}

//we use bootstrap popover
function updatePopover(popoverId,srcLang,destLangISO,enId,src,pid,row,$rootScope){
    var url = '/frengly/data/getByEnId?srcLang='+srcLang+'&destLang='+destLangISO+'&enId='+enId+'&srcWord='+src;
    
    //console.log('updating data for popoverId:'+popoverId);
    $.ajax({
    		url: url,    		
    		contentType: 'application/json;charset=utf-8',						    		
    		success: function(d) {
    				
			    	var html = '';
			    	$.each(d.list, function(i, row) {
		        		var pid = row.pid;
		        		
		        		//TODO: display also priority (cases or aspect) and update it on setPriority
		        		var delIcon = '<span><img src="images/trash.png" class="centered-image" style="cursor:pointer;" onclick="deleteByPid(\''+srcLang+'\',\''+destLangISO+'\','+pid+',\''+row.srcWord+'\','+row.en_id+','+popoverId+')" /></span>';					        		
		        		
		        		html=html+delIcon+'&nbsp;<span onclick="setPriority(\''+srcLang+'\',\''+destLangISO+'\','+pid+',\''+row.srcWord+'\','+row.en_id+','+popoverId+','+$rootScope+')" data-enId="'+row.en_id+'" id="'+ pid +'">'+row.destWord+'&nbsp; <span data-priority="'+pid+'" style="cursor: pointer; font-weight: bold">'+row.priority+'</span></span><br/>';
						//var priorLink = '<a onclick="setPriority('+digit+','+alignmentObjId+',\''+pidRow+'\',\''+enId+'\',\''+srcLang+'\',\''+destLang+'\',\''+srcWord+'\')">'+word+aspect+'</a>';					
		        	});
			    	
			    	
			    	html=html+'<input id="inputBtnAdd" onkeydown="addPair(event,\''+srcLang+'\',\''+destLangISO+'\',\''+pid+'\',\''+src+'\',\''+enId+'\','+popoverId+')" class="form-control" data-srcWord="'+src+'"  data-srcLang="'+srcLang+'" data-destLang="'+destLangISO+'" type="text" />';
			    	//$('#'+popoverId).html(html);
			    	$('.popover').each( function() {
			    		$(this).find(".popover-content").empty().append(html);	
			    		//$(this).popover({html: true,placement: 'bottom',content: html}).popover('show');
			    	});
			    	$( "#inputBtnAdd" ).focus();			    	
			    	//.data().popover.tip().hasClass('in')
			        
    		}
    });
    
}

function addClass(obj){	
	var id = obj.id.split('__')[1];
	var enId = obj.id.split('__')[2];
	//log.info(id);
	$('#left__'+id).addClass('hov');
	$('#right__'+id+'__'+enId).addClass('hov');
}

function removeClass(obj){
//	if (isOpen==1) return; 
	var id = obj.id.split('__')[1];
	var enId = obj.id.split('__')[2];
	//log.info(id);
	$('#left__'+id).removeClass('hov');
	$('#right__'+id+'__'+enId).removeClass('hov');
}


function scrollTopAsBottom(){	
	var pos = $('#display-box').scrollTop();
	$('#input-box').scrollTop(pos);
}


function detect2($scope,$http,$rootScope){
	 if ($rootScope.autodetection==false) {return;}
	 var callURL = '/frengly/data/detect';
	 
			 //Ajax calls grouping to avoid flooding server
			 var tokens = $scope.text.split(' ');
			 var lastWord = tokens[tokens.length-1];
			 //console.log('last word:'+lastWord+',length:'+lastWord.length);
			 if (lastWord.length<3) {
				 //console.log('last word is to short to detect');
				 return;
			 }
			 //Ajax calls grouping to avoid flooding server
	 
	 //console.log('text for detection:::::'+$scope.text);
	 $.ajax({
			url: callURL,
			method: 'post',
			data: {
		 		text : $scope.text
			},
			success: function(data){	
				$rootScope.srcLang = data.detectedLabel;
				$rootScope.$apply();
			}
		});
	
		
}


function deleteByPid(srcLang,destLang,pid,srcWord,enId,popoverId,$rootScope) {
	
/*	var vals = $('#board-title').text();
	if (vals.indexOf('Autodetected')>-1){
		vals = vals.split(' ')[1];
	}
	destLang = vals;*/
	var callURL = '/frengly/data/deleteDictEntry?pid='+pid+'&srcLang='+srcLang+'&destLang='+destLang;
	$.ajax({
		url: callURL,
		method: 'delete',
		success: function(data){	
			updatePopover(popoverId,srcLang,destLang,enId,srcWord,pid,$rootScope);
		}
	});
}

//if destLang=EN then pid and enId is useless, thats why u need to use word
function setPriority(srcLang,destLang,pid,srcWord,enId,popoverId,$rootScope) {	
	var callURL = '/frengly/data/priority';
	$.ajax({
		url: callURL,
		method: 'post',
		data: {
			srcLang: srcLang,
			destLang: destLang,
			pid: pid,
			srcWord: srcWord,
			enId: enId
		},
		success: function(data){	
			updatePopover(popoverId,srcLang,destLang,enId,srcWord,pid,$rootScope);
			//loadPrioritiesBasic(digit,alignmentObjId,enId,srcLang,destLang,srcWord);
		}
	});
}


/*$('a.popup-ajax').popover({
    "html": true,
    "content": function(){
        var div_id =  "tmp-id-" + $.now();
        return details_in_popup($(this).attr('href'), div_id);
    }
});
*/
/*function loadPopoverContent(id){
	var id = id.split('__')[0];
	console.log('Loading popover content for id:'+id);
	

    $.ajax({
        url: '/frengly/static/popover.json?'+id,
        dataType: 'json',
        success: function(data) {        	    	
        	$.each(data, function(i, row) {
        		var pid = row.pid;        		
        		html=html+'<div id="'+ pid +'">Loading...'+pid+'</div>';
        		//console.log(html);
        	});
        	console.log(html);
        	
        	return html;
        }
    });
    
   
}*/


function addPair(e,srcLang,destLang,pid,srcWord,enId,popoverId,$rootScope){

	if (e.keyCode == '13') {
	console.log('addPair');	
    
	
/*	var inputId = $('*:focus').attr('id');
	var text = $('#text_'+inputId).html();*/
	
	var translation = $('*:focus').val();
	var srcLang=$('*:focus').attr('data-srcLang');
	var destLang=$('*:focus').attr('data-destLang');
	var srcWord=$('*:focus').attr('data-srcWord');

	console.log('*srcLang:'+srcLang);
	console.log('*destLang:'+destLang);
	console.log('*srcWord:'+srcWord);
	console.log('*translation:'+translation);
	
	var url = '/frengly/data/addPairREST';
	$.ajax({
		url: url,
		method: 'post',
		data: JSON.stringify({
			isJason: 'true',
			//status: '3418',//91			
			srcWord: srcWord,
			destWord: translation,
			src: srcLang,
			dest: destLang
		}),
		dataType: 'json',		
        contentType: 'application/json;charset=utf-8',        
	  	success: function(data){
			try{
				console.log('Server said inserted!!!!!!!!!!!!!!!!!!');
				var msg='';
				if (data.newId>0){
					msg = 'Inserted new word with id: '+data.newId;
					getFiscalCounter();
				} else if (data.newId==-2){
					msg = 'Translation not insterted';
				} else if (data.newId==-3){
					msg = 'Incorrect alphabet of translation';
				} else if (data.newId==-4){
					msg = 'Incorrect translation';
				} else if (data.newId=='null'){
					msg = 'Translation already exists';
				}
				
				//getNextEnglish();
				
//				$("#yellowMsg").html(msg);
//				$("#yellowLabel").show().delay(2000).fadeOut();			
//				$('#foreignAdd').val('');			
//				$('#englishAdd').val('');
			} catch(ex){
				//alert(ex);
			}
			updatePopover(popoverId,srcLang,destLang,enId,srcWord,pid,$rootScope);

	  	},	  	
		error: function (errormessage) {
	  		//alert(errormessage.responseText);
        }
	});	
	
	}//end enter
}






function handlepaste(elem, e, $scope,$http,$rootScope) {
    var savedcontent = elem.innerHTML;
    //console.log(e+','+e.clipboardData+','+e.clipboardData.getData);
    if (e && e.clipboardData && e.clipboardData.getData) {// Webkit - get data from clipboard, put into editdiv, cleanup, then cancel event
        console.log('YESSS!!!');
    	if (/text\/html/.test(e.clipboardData.types)) {
            elem.innerHTML = e.clipboardData.getData('text/html');
        }
        else if (/text\/plain/.test(e.clipboardData.types)) {
            elem.innerHTML = e.clipboardData.getData('text/plain');
        }
        else {
            elem.innerHTML = "";
        }
    	
        waitforpastedata(elem, savedcontent,$scope,$http,$rootScope);
        if (e.preventDefault) {
                e.stopPropagation();
                e.preventDefault();
        }
        return false;
    }
    else {// Everything else - empty editdiv and allow browser to paste content into it, then cleanup
        elem.innerHTML = "";
        waitforpastedata(elem, savedcontent,$scope,$http,$rootScope);
        return true;
    }
}

function waitforpastedata(elem, savedcontent,$scope,$http,$rootScope) {
	console.log('waitforpastedata...');
	console.log('$rootScope::'+$rootScope);

    if (elem.childNodes && elem.childNodes.length > 0) {
        processpaste(elem, savedcontent,$scope,$http,$rootScope);
        //detect();
    }
    else {
        that = {
            e: elem,
            s: savedcontent
        }
        that.callself = function () {
            waitforpastedata(that.e, that.s, $scope,$http,$rootScope)
        }
        setTimeout(that.callself,20);
    }
}

function processpaste (elem, savedcontent,$scope,$http,$rootScope) {
	console.log('processpaste');
	console.log('$rootScope::'+$rootScope);
	console.log($scope+','+$http+','+$rootScope);
	pasteddata = elem.innerHTML;	
	/*elem.innerHTML = $("<div />", { html: pasteddata }).text().replace(/(\r\n|\n|\r)/gm, "");*/
	elem.innerHTML = $("<div />", { html: pasteddata }).text().replace(/(\r\n|\n|\r)/gm, "");	
	//and the same for top layer
	$('#input-box-over-in')[0].innerHTML=$("<div />", { html: pasteddata }).text().replace(/(\r\n|\n|\r)/gm, "");
	$scope.text = $("<div />", { html: pasteddata }).text().replace(/(\r\n|\n|\r)/gm, "");
	detect2($scope,$http,$rootScope);
	
	
}