demoApp.controller('LogoutController', ['$location', '$cookieStore', '$rootScope', 'authentication', function ($location, $cookieStore, $rootScope, authentication) {	
  
  //SUCESS
  var success = function(data){    
	 console.log('logout was successfull!!!!!!!!!!!');
     toastr.info('User logout');
     $location.path('/login');
  }	    
  //ERROR
  var error = function(data){    	
  	 toastr.error('Could not logout user');    	  
  }
  
  //LOGIN
  authentication.logout().success(success).error(error);
}]);