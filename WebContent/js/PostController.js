demoApp.controller('PostController', ['$scope', '$routeParams', '$rootScope', '$location', '$http', '$cookieStore', 'postManager', function ($scope, $routeParams, $rootScope, $location, $http, $cookieStore, postManager) {	
	console.log('inside post controller');
	
	$scope.addPost = function() {        
		
		var data = {
				topicId: $cookieStore.get('topicId'),
				postText: $scope.postText
		};
		
		var success = function(data){
			$scope.loadPosts();	
		}
		
		var error = function(data){ 
		}
		
		postManager.create(data).success(success).error(error);
	}
	
	$scope.deletePost = function(id){
		console.log("deleting post with id:"+id);
		$http['delete']('/frengly/data/post/'+id).success(function(data, status, headers, config) {$scope.loadPosts();}).error(function(data, status, headers, config) {});		
	}	
	
	$scope.loadPosts = function() {
		console.log('loading posts with loadPosts() method '+$routeParams.tid);
		var topicId = $routeParams.tid != null ? $routeParams.tid : $cookieStore.get('topicId');
		$cookieStore.put('topicId', topicId);
		$http.get('/frengly/data/topic?topicId='+topicId).success(function(data, status, headers, config) {$scope.posts = data.posts;$scope.topicName = data.topicName;}).error(function(data, status, headers, config) {});		
	}
	
	$scope.loadPosts();
	
}]);



