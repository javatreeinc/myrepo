var demoAppControllers = angular.module('historyControllers', []);

demoAppControllers.controller('HistoryController', ['$scope', '$location', function ($scope, $location) {	
	
	$scope.selectHistoryLine = function(kendoEvent) {	
		 try{
			 var grid = kendoEvent.sender;
		     var selectedData = grid.dataItem(grid.select());
		     console.log('done1:'+selectedData+"::"+selectedData.id);
		     
			
		     var tr = grid.tbody.find("tr[data-uid='" + selectedData.uid + "']");	     
		     var td = $(tr).find("td:first");
		     var src = $(td).children('img').attr('src');
		     //alert(src);
		     if (src.indexOf('star1')>0){
		    	 $(td).children('img').attr('src','images/star0.png');
		     } else {
		    	 $(td).children('img').attr('src','images/star1.png');
		     }
		     console.log('done3');
		 }catch(ex){}
	};

    $scope.mainGridOptions = {
            dataSource: {   
        		type: "json",
                transport: {
                    read: {
    	    			url: "/frengly/data/history",
    	    			contentType: "application/json; charset=utf-8",
    	                dataType: "json",
    	                type: "GET"
                    }
                },
                pageSize: 15,
                schema: {
                	data: "list",
                	total: "counter"
                }
            },
            dataBound: function () {
                displayNoResultsFound($('.frenglyGrid'),'historyEmpty');
            },
            scrollable: false,
            sortable: true,
            editable: false,
            groupable: false,
            selectable: true,
            pageable: { buttonCount: 4 },
            columns: [
                      { name: "image", title: "select", width: "2%", template: '<img data-ng-click="selectHistoryLine(this)" src="images/star0.png" />', headerAttributes: {style: "display:none"}, attributes: {style: "border-left: 1px solid rgb(230,230,230);"}},                     
                      { field: "text", width: "40%", headerAttributes: {style: "display:none"} },
                      { field: "translated", width: "40%", headerAttributes: {style: "display:none"} },
                      { field: "langSrc", width: "5%", template: '${langSrc}-${langDest}', headerAttributes: {style: "display:none"}, attributes: {style: "border-right: 1px solid rgb(230,230,230);"} },
            ],
            change: function (item) {
                var selected = $.map(this.select(), function (item) {
                    return $(item).find('td').first();
                });
                alert($(selected).prop('tagName'));
            },
    };
}]);



