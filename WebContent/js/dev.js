//this is only for DEV, to connect to frengly.com rest
$.ajaxPrefilter(function(options) {				  
	  //only for localhost
	  if (location.host.indexOf('royalwebhosting')>-1 && options.url.indexOf('frengly.com')<0){
		  options.url = "http://frengly.com" + options.url.substring(8); 
		  console.log('[PRE_FILTER] ajaxPrefilter:'+options.url);
		  options.crossDomain = true; 
	  }				   
});
  
//only for DEV
demoApp.factory('httpRequestInterceptor', function () {
	  return {
	    request: function (config) {		  
		  if (location.host.indexOf('royalwebhosting')>-1 && config.url.indexOf('frengly.com')<0 && (config.url.indexOf('static')>-1 || config.url.indexOf('data')>-1)){
			  config.url = "http://frengly.com" + config.url.substring(8); 			  			  
		  }
	      return config;
	    }
	  };
});
	 
demoApp.config(['$httpProvider',function ($httpProvider) {
	  $httpProvider.interceptors.push('httpRequestInterceptor');
}]);