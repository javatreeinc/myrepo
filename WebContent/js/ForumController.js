demoApp.controller('ForumController', ['$http', '$scope', '$rootScope', '$location', '$cookieStore', 'topicManager', function ($http,$scope, $rootScope, $location, $cookieStore, topicManager) {	

	//$http['delete']()
	$scope.deleteTopic = function(id){		
		$http['delete']('/frengly/data/topic/'+id).success(function(data, status, headers, config) {$scope.loadTopicNew();}).error(function(data, status, headers, config) {});		
	}	
	
	$scope.addTopic = function() {        
		
		var data = {
			topicName: $scope.topicName
		};
		
		var success = function(data){			
			//loadTopics($scope);
			//$scope.topicGrid.refresh();
			$scope.loadTopicNew();
		}
		
		var error = function(data){ 
		}
		
		topicManager.create(data).success(success).error(error);
	}
	
/*	$scope.selectTopic = function(id) {		
		
		$rootScope.topicId = id;
		
		//you have to put topicId in cookie, because after reload $rootScope is lost
		$cookieStore.put('topicId', id);
		
		$location.path('/post');
	};*/

	//loadTopics($scope);
	
	$scope.loadTopicNew = function() {
		$http.get('/frengly/data/forum?getTopics=1').success(function(data, status, headers, config) {$scope.topics = data.topics;$scope.topicName = data.topicName;}).error(function(data, status, headers, config) {});
	}
	
	$scope.loadTopicNew();

}]);