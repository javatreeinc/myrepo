demoApp.controller('ContactController', ['$scope','$location', '$cookieStore', 'UserDBService', function ($scope, $location, $cookieStore, UserDBService) {	
    $scope.send = function() {
    	toastr.error('Thank you for sending us message. We will reply to you today unless some unknown force stops us.');
    };
}]);
