demoApp.controller('ContestController', ['$http','$rootScope','$scope','$location','$compile','contribution', function ($http,$rootScope,$scope,$location,$compile,contribution) {	
	//loadMissing();	
	if ($rootScope.srcLangContest==null) $rootScope.srcLangContest = 'English';
	if ($rootScope.destLangContest==null) $rootScope.destLangContest = 'French';
	
	$scope.categories = [{name:'Tech'},{name:'Med'},{name:'Lifestyle'},{name:'Sport'},{name:'Gangsta'},{name:'Kitchen'},{name:'History'},{name:'Law'},{name:'Other'}];
	$scope.myCategory = $scope.categories[4];

	//init category on the server
	$http.get('/frengly/data/changeCategory/'+$scope.myCategory.name).success(function(data, status, headers, config) {}).error(function(data, status, headers, config) {});
	
	//$scope.count = 0;
	$scope.openModal = function(){
		$("#textToUploadAndAnalyze").html('');
	};
	
	$scope.switchContestLangs = function(){		
		var buf = $rootScope.srcLangContest; 
		$rootScope.srcLangContest = $rootScope.destLangContest;
		$rootScope.destLangContest = buf;
		loadContestGrid($scope,$rootScope,$compile);			
		forUpload($scope,$rootScope);
	}
	
	$scope.delTop = function(){		
		$http['delete']('/frengly/data/deleteTop?subAction=deleteWholeTable&srcLang='+$rootScope.srcLangContest).success(function(data, status, headers, config) {loadContestGrid($scope,$rootScope,$compile);}).error(function(data, status, headers, config) {});		
	};
	
	$scope.delTopRow = function(id){		
		$http['delete']('/frengly/data/deleteTopRow?subAction=deleteTopRow&srcLang='+$rootScope.srcLangContest+'&id='+id).success(function(data, status, headers, config) {loadContestGrid($scope,$rootScope,$compile);}).error(function(data, status, headers, config) {});		
	};
	
	$scope.changeCategory = function(option){		
		$http.get('/frengly/data/changeCategory/'+option.name).success(function(data, status, headers, config) {}).error(function(data, status, headers, config) {});		
	};
	
	$scope.saveMap = function(){		
		$http.get('/frengly/data/storeMap').success(function(data, status, headers, config) {$('#myModal').modal('hide');loadContestGrid($scope,$rootScope,$compile); }).error(function(data, status, headers, config) {});		
	};
	
	$scope.changeSrcContestLang = function(key,value){
		console.log('change src contest lang:'+key+' '+value);
		var buf = $rootScope.srcLangContest;
		$rootScope.srcLangContest = value;
		if ($rootScope.srcLangContest!='English' && $rootScope.destLangContest!='English') {
			$rootScope.destLangContest='English';	
		}
		if ($rootScope.srcLangContest=='English' && $rootScope.destLangContest=='English'){
			$rootScope.srcLangContest='English';
			$rootScope.destLangContest=buf;			
		}
		loadContestGrid($scope,$rootScope,$compile);
	};
	
	$scope.changeDestContestLang = function(key,value){
		console.log('change dest contest lang:'+key+' '+value);
		var buf = $rootScope.destLangContest;
		$rootScope.destLangContest = value;
		if ($rootScope.srcLangContest!='English' && $rootScope.destLangContest!='English') {
			$rootScope.srcLangContest='English';	
		}
		if ($rootScope.srcLangContest=='English' && $rootScope.destLangContest=='English'){
			$rootScope.destLangContest='English';
			$rootScope.srcLangContest=buf;			
		}
		loadContestGrid($scope,$rootScope,$compile);
	};
	
	$scope.addTopTran = function(id){
		
		var text=$('#'+id).attr('data-text');
		var id=$('#'+id).attr('id');
		var translation=$('#'+id).val();		
	        	
		//DATA
        var data = {
    		isJason: 'true',		
			srcWord: text,
			destWord: translation,
			src: $rootScope.srcLangContest,
			dest: $rootScope.destLangContest,
			id: id
        }

        //SUCCESS
        var success = function(data){
        	loadContestGrid($scope,$rootScope,$compile);	         
	    }
	    
        //ERROR
	    var error = function(data){    	
	    	toastr.error('Invalid add Pair operation');    	  
	    }      
	    
	    //ADD PAIR		
	    contribution.addPair(data).success(success).error(error);
		
		
		

		

	}
	
/*	$scope.loadContest = function(){		
		loadContestGrid($scope);
	};*/
	
	forUpload($scope,$rootScope);
	//$("#categ1").kendoDropDownList();
	loadContestGrid($scope,$rootScope,$compile);
	

    
    
	
}]);


function loadContestGrid($scope,$rootScope,$compile){
	
	//it was added to refresh titles of columns after ajax reload!!!
	//specifically when you change dropdown langs
	try{
		$('#mygrid').data().kendoGrid.destroy();
		$('#mygrid').empty();
	}catch(ex){}
	
	var callURL = "/frengly/data/contest?subAction=loadMissing&srcLang="+$rootScope.srcLangContest+"&destLang="+$rootScope.destLangContest;
    
	$("#mygrid").kendoGrid({
        dataSource: {   
    		type: "json",
            transport: {
                read: {
	    			url: callURL,
	    			contentType: "application/json; charset=utf-8",
	                dataType: "json",
	                type: "GET",
	                complete: function (data,status) {
				    	if (status === "success") {	    					
 
	    					var result = jQuery.parseJSON(data.responseText);
	    					$scope.score = result.score;
	    					$rootScope.message = 'You have contributed '+result.score+' translations';
	    					
	    					//necessary so as the progress score is updated immediately
	    					$rootScope.$apply();
	    					
	    					//necessary for the jquery components created without angular have the ng-click etc.
	    					$compile($('.k-input'))($scope);
	    					$compile($('.k-button'))($scope);
	    					
				        }
                	}
                }
            },
            pageSize: 10,            
            schema: {
            	data: "rows",
            	total: "counter" //if server doesnt deliver this value, 0 will be displayed
            }
        },
        dataBound: function () {        	
            displayNoResultsFound($('#mygrid'),'contestEmpty');
        },
        scrollable: false,
        sortable: true,
        editable: false,
        groupable: false,
        pageable: { buttonCount: 4 },
        columns: [
            { field: "id", width: "70px" },
            { field: "category", width: "20%",template: "<span title=\"#=category.split('=')[1]#\" class=\"label label-default\">#=category.split('=')[0]#</span>" }, //http://www.tutorialspoint.com/bootstrap/bootstrap_labels.htm
            { field: "text", title: $rootScope.srcLangContest, width: "20%" },
            { field: "translation", title: $rootScope.destLangContest, width: "20%",template: "<input id=\"${id}\" data-srcLang="+$rootScope.srcLangContest+" data-destLang="+$rootScope.destLangContest+" data-text=\"${text}\" ng-enter=\"addTopTran('${id}')\" class=\"k-input k-textbox\" data-bind='value: age' data-role='numerictextbox'>" },
            { field: "qty"},
            { field: "en_id"},
            { field: "uploads"},
            { field: "action", template: "<input type=\"button\" class=\"k-button\" ng-show=\"roles=='admin'\" ng-click=\"delTopRow('${id}')\" value=\"del\" />" }
            
  
        ]
    });	    
}



function capitaliseFirstLetter(string){
    return string.charAt(0).toUpperCase() + string.slice(1);
}



function forUpload($scope,$rootScope){
	var uploadURL = '/frengly/data/uploadText?subAction=uploadFile';
	$("#files").kendoUpload({
        async: {            
        	saveUrl: uploadURL,
            //removeUrl: rootURL+'controller?action=getMissingWord&subAction=uploadFile&srcLang='+srcLang+'&destLang='+destLang,
            autoUpload: true
        },
        upload: function (e) {
            e.data = { srcLang: $rootScope.srcLangContest, destLang: $rootScope.destLangContest };
        },       
        success: attachOnSuccess,
		complete: onCompleteUpload
    });
}

function attachOnSuccess(e){
	$(".k-upload-files.k-reset").find("li").remove();
	$(".k-widget.k-upload.k-header").addClass("k-upload-empty");
	
	var text = e.XMLHttpRequest.responseText;	
	var result = jQuery.parseJSON(text);
	var map = result.map;
	var mapId = result.mapId;
	
	var htmlCode="";
	$.each(map, function (i, val) {
		console.log(i+"="+val);
		htmlCode+="<span style=\"cursor: pointer\" onClick=\"deleteFromSessionMap(\'"+i+"\',this)\" class=\"label label-primary\">"+i+"("+val+")</span> "
	});
	
	$("#textToUploadAndAnalyze").html(htmlCode);
	
}

function deleteFromSessionMap(word,spanObj){
	console.log('deleting word in server memory map');
	
	$.ajax({
			url: '/frengly/data/excludeFromMap',
			method: 'post',
			data: {
		 		text : word
			},
			success: function(data){
				$(spanObj).remove();
			}
	});
}

function onChangeC(e){
	
}

function onCompleteUpload(e) {
	//loadMissing();
}