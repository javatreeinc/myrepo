demoApp.controller('HeaderController', ['$scope', '$rootScope', '$location', 'authentication', 'badges', function ($scope, $rootScope, $location, authentication, badges) {
	
	$scope.isActive = function (viewLocation) {
		//console.log('active........................'+viewLocation);
	    var active = (viewLocation === $location.path());
	    return active;
	};
	
	
	$scope.delAccount = function () {
		console.log('deleting account');
		     
        //SUCCESS
        var success = function(data){        	
	          toastr.info('Account deleted');	          
	          authentication.logout();
	    }	    
        //ERROR
	    var error = function(data){    	
	    	 toastr.error('Could not remove account');    	  
	    }
	    
	    //LOGIN
	    authentication.deleteAccount().success(success).error(error);
		
	};
	
	
	$scope.loadBadges = function () {
		     
        //SUCCESS
        var success = function(data){
        	$rootScope.badgeHistory = data.todayHistory;
        	$rootScope.badgeForum = data.todayTopics;
	    }	    
        //ERROR
	    var error = function(data){    	    	  
	    }
	    
	    //load badges
	    badges.load().success(success).error(error);
		
	};
	
	
	$scope.loadBadges();

}]);