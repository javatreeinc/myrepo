var demoAppControllers = angular.module('rankControllers', []);

demoAppControllers.controller('RankController', ['$location', '$scope', '$compile', '$http', function ($location, $scope, $compile, $http) {	
	
	printChamps();
	loadRank2($scope,$compile);	
	
	$scope.delLiveTranslation = function(id){		
		$http['delete']('/frengly/data/deleteLiveTranslation?id='+id).success(function(data, status, headers, config) {loadRank2($scope,$compile);}).error(function(data, status, headers, config) {});		
	};

	
	$scope.refreshRank = function(){		
		loadRank2($scope,$compile);
		printChamps();
	}
}]);

function loadRank2($scope,$compile){
	
	var callURL = "/frengly/data/bestTranslators";
	$("#rankGrid").kendoGrid({
        dataSource: {   
    		type: "json",
            transport: {
                read: {
	    			//url: "/frengly/static/rank.json",
					url: callURL,	
	    			contentType: "application/json; charset=utf-8",
	                dataType: "json",
	                type: "GET",
	                complete: function (data,status) {
				    	if (status === "success") {	    					
 	    					
	    					
//	    					$rootScope.$apply();
	    					
	    					
	    					$compile($('.k-input'))($scope);
	    					$compile($('.k-button'))($scope);
	    					
				        }
                	}
                }
            },
            pageSize: 10,
            schema: {
            	data: "list",
            	total: "counter" //if server doesnt deliver this value, 0 will be displayed
            }
        },
        dataBound: function () {
            displayNoResultsFound($('#rankGrid'),'rankEmpty');
        },
        scrollable: false,
        sortable: true,
        editable: false,
        groupable: false,
        pageable: { buttonCount: 4 },
        columns: [
            { field: "id", width: "70px" },
            { field: "newId", width: "70px" },
            
            { field: "source", width: "20%"},
            { field: "translation", width: "20%"},
            { field: "username"},
            { field: "time", width: "10%" },            
            { field: "x_top_lang"},
            { field: "up", width: "30px"},
            { field: "down", width: "30px"},
            { field: "action", template: "<input type=\"button\" class=\"k-button\" ng-show=\"roles=='admin'\" ng-click=\"delLiveTranslation('${id}')\" value=\"del\" />" }
            
        ]
    }).find('> table').addClass("table");  
}
function printChamps(){
	var callURL = "/frengly/data/bestTranslators";
	$.ajax({
		//url: rootURL+'controller?action=getMissingWord&subAction=loadMissing&operation='+operation+'&srcLang='+srcLang+'&destLang='+destLang,
		url: callURL,
	    dataType: 'json',
	    contentType: 'application/json;charset=utf-8',
	    method: 'get',
	    success: function(data){
	    	
	    	if (data.topList.length==0) {
	    		$('#champion').hide();	
	    		return;
	    	}
	    	
			var idx = 0;
			$('#champion').html('');			
			$.each(data.topList, function(i, row) {
				idx++;
				var pos = idx+'.';
				var decoration='';
				if (idx==1) {pos='1st ';decoration='font-size:20px;';}
				if (idx==2) {pos='2nd ';decoration='font-size:18px;';}
				if (idx==3) {pos='3rd ';decoration='font-size:16px;';}
				var counter = row.counter;
				var usernameStr = row.username;
				//console.log(usernameStr);
				if (usernameStr.indexOf('@')){
					usernameStr = usernameStr.split('@')[0];
				}
				$('#champion').append('<span style="'+decoration+'margin-left: 10px;">'+pos+usernameStr+'('+counter+') </span>');				
			});
		}
	});
	
}