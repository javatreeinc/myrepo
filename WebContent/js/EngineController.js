demoApp.controller('EngineController', ['$http', '$scope', '$rootScope', '$location', '$cookieStore', 'topicManager', function ($http,$scope, $rootScope, $location, $cookieStore, topicManager) {	

	$scope.loadStats = function() {		
		$http.get('/frengly/data/engine').success(function(data, status, headers, config) {$scope.stats = data.list;}).error(function(data, status, headers, config) {});
	}

	$scope.loadStats();

}]);