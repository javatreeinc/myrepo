//'kendo.directives' is needed only because of special grid in history.html page!!!!
var demoApp = angular.module('demoApp',['ngRoute','ngCookies','kendo.directives','qualityControllers','rankControllers','userControllers','privacyControllers','historyControllers','angular-google-analytics','nsPopover']);

demoApp.config(['$routeProvider',function ($routeProvider){
	console.log("app.js config");
	$routeProvider
		.when('/',{redirectTo : '/translate'})
		//.when('/home',{controller: 'SimpleController',templateUrl: 'partials/home.html'})
		
		.when('/translate',{controller: 'TranslateController',templateUrl: 'partials/translate.html', title: 'Free Online Translator'})						
		.when('/translate2',{controller: 'TranslateController2',templateUrl: 'partials/translate2.html', title: 'Free Online Translator'})							
		.when('/forum',{controller: 'ForumController',templateUrl: 'partials/forum.html', title: 'Translators Forum'})
		.when('/post/:tid?/:tin?',{controller: 'PostController',templateUrl: 'partials/post.html', title: 'Translator Forum Topic'})
		.when('/api',{controller: 'SimpleController',templateUrl: 'partials/api.html', title: 'Translation Web Service API'})
		.when('/privacy',{controller: 'PrivacyController',templateUrl: 'partials/privacy.html'})
		.when('/buy',{controller: 'SimpleController',templateUrl: 'partials/buy.html'})		
		.when('/history',{controller: 'HistoryController',templateUrl: 'partials/history.html', title: 'Translation History'})
		.when('/quality',{controller: 'QualityController',templateUrl: 'partials/quality.html', title: 'Machine Translation Quality Index'})
		.when('/contribute',{controller: 'SimpleController',templateUrl: 'partials/contribute.html', title: 'Translation Engine Improvement'})
/*		.when('/rank',{controller: 'RankController',templateUrl: 'partials/rank.html'})
		.when('/contest',{controller: 'ContestController',templateUrl: 'partials/contest.html'})
*/		
		.when('/register',{controller: 'RegisterController',templateUrl: 'partials/register.html'})
		.when('/user',{controller: 'UserController',templateUrl: 'partials/user.html'})		
		.when('/contact',{controller: 'ContactController',templateUrl: 'partials/contact.html'})
		.when('/login',{controller: 'LoginController',templateUrl: 'partials/login.html'})				
		.when('/browser',{controller: 'BrowserController',templateUrl: 'partials/browser.html'})
		.when('/engine',{controller: 'EngineController',templateUrl: 'partials/engine.html'})
		.when('/logout', {template: " ", controller: "LogoutController"})			
		.otherwise({redirectTo: '/translate'});
}]);


demoApp.run(['$http','$rootScope', '$cookieStore', '$location', 'UserDBService',function ($http,$rootScope, $cookieStore, $location, UserDBService) {

	  // enumerate routes that don't need authentication
	  //var secured = ['/forum'];
	  var secured = [];
	   
	  // check if current location matches route  
	  var routeClean = function (route) {
	    return _.find(secured,
	      function (noAuthRoute) {
	        return _.str.startsWith(route, noAuthRoute);
	      });
	  };
	  
	  $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
			//TODO: check why there is an error (after logout) when you remove this line
		    if (current.$$route==null) return;
	        $rootScope.title = current.$$route.title;
	        if ($rootScope.title==null) {
	        	$rootScope.title = 'Free Online Translator';
	        }
	  });

		  //---------------- SECURITY
		  $rootScope.$on('$routeChangeStart', function (event, next, current) {
		    // if route requires auth and user is not logged in
		      
			  //google analytics
			  //ga('send', 'pageview', {'page': $location.path()});
			  
			  var usernameFromCookie = $cookieStore.get('rememberme');			  
			  var rolesFromCookie = $cookieStore.get('roles');
			  
		      //AUTOLOGIN - dont autologin when logging out
		      if (usernameFromCookie!=null && $location.url().indexOf('logout')<0){		    	  
		    	  $rootScope.username = usernameFromCookie;
		    	  $rootScope.roles = rolesFromCookie;
		    	  //TODO: load from server user specific data, example: his photo
		    	  //put this user into session on server
		    	  $http.post('/frengly/data/autologin',{username: usernameFromCookie});
		      }
		      
		      
		    if (routeClean($location.url()) && $rootScope.username==null) {	    	    
			    
			    //store old location
			    $rootScope.oldLocation = $location.url();
			    
			    //redirect
			    $location.path('/login');
			    
		    } else {
		    	console.log($location.url()+'**alllowed');
		    }
		  });
		  //---------------- SECURITY
		  
		  
		  //it will be used by translate.html and contest.html
		  if ($rootScope.langs==null){
		  $http.get('/frengly/static/langs.json')
		  .success(function(data, status, headers, config) {
			  $rootScope.langs = data.list;})
		  .error(function(data, status, headers, config) {});
		  }
	  
	}]);

//its necessary for main input box
//cannot be 'contenteditable' as it would overlap with contenteditable html5 property
demoApp.directive("contenteditables", function() {
	  return {
	    restrict: "A",
	    require: "ngModel",
	    link: function(scope, element, attrs, ngModel) {

	      function read() {
	        ngModel.$setViewValue(element.html());
	      }

	      ngModel.$render = function() {
	        element.html(ngModel.$viewValue || "");
	      };

	      element.bind("blur keyup change", function() {
	        scope.$apply(read);
	      });
	    }
	  };
	});


demoApp.directive('ngEnter', function () {
    return {
       restrict: 'A',
       link: function (scope, elements, attrs) {
          elements.bind('keydown keypress', function (event) {
              if (event.which === 13) {            	  
                  scope.$apply(function () {
                      scope.$eval(attrs.ngEnter, {'event': event});
                  });
                  event.preventDefault();
              }
          });
       }
    };
});


demoApp.directive("myMethod",['$parse',function($parse) {
    var directiveDefinitionObject = {
      restrict: 'A',
      scope: { method:'&myMethod' },
      link: function(scope,element,attrs) {
         var expressionHandler = scope.method();
         var id = "123";

         $(element).click(function( e, rowid ) {
            expressionHandler(id);
         });
      }
    };
    return directiveDefinitionObject;
}]);

demoApp.directive('ngReallyClick', [function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('click', function() {
                var message = attrs.ngReallyMessage;
                if (message && confirm(message)) {
                    scope.$apply(attrs.ngReallyClick);
                }
            });
        }
    }
}]);
	

function displayNoResultsFound(grid,messageBoxId) {
    // Get the number of Columns in the grid
    var dataSource = grid.data("kendoGrid").dataSource;
    var msg = $("#"+messageBoxId);
    // If there are no results place an indicator row
    if (dataSource._view.length == 0) {
    	grid.hide();    	
    	msg.show();
    } else {
    	grid.show();    
    	msg.hide();
    }
}

var interceptor = function ($q, $location) {
    return {
        request: function (config) {
            console.log(config);
            return config;
        },

        response: function (result) {
            console.log('Repos:');
            result.data.splice(0, 10).forEach(function (repo) {
                console.log(repo.name);
            })
            return result;
        },

        responseError: function (rejection) {
            console.log('Failed with', rejection.status, 'status');
            if (rejection.status == 403) {
                $location.url('/login');
            }

            return $q.reject(rejection);
        }
    }
};