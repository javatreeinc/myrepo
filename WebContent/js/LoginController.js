demoApp.controller('LoginController', ['$scope', '$rootScope', '$location', '$cookieStore', 'authentication', 'UserDBService', function ($scope, $rootScope, $location, $cookieStore, authentication, UserDBService) {	
   
	$scope.loginUser = function() {
      
   
		//CREDS
        var credentials = {
    		username: $scope.user.email,
    		password: $scope.user.password,
    		rest: '1'
        }

        //SUCCESS
        var success = function(data){
        	if (data.username!=null){	
	    	  $rootScope.username = data.username;
	    	  $rootScope.roles = data.roles;
	    	  
	    	  $location.path($scope.oldLocation);          
	          if ($scope.oldLocation==null){
	        	  $location.path('/home');
	          }
	          
	          //store the cookie for autologin
	          $cookieStore.put('rememberme', data.username);
	          $cookieStore.put('roles', data.roles);
	          
	          //send notification
	          toastr.info('Logged in successfully');
        	} else {
        		toastr.error('Invalid credentials or you are 6 finger man');
        	}
	          
	    }
	    
        //ERROR
	    var error = function(data){    	
	    	 toastr.error('Invalid credentials or you are 6 finger man');    	  
	    }
      
	    
	    //LOGIN
	    authentication.login(credentials).success(success).error(error);
      
      
    };
}]);
