var demoAppControllers = angular.module('qualityControllers', []);

demoAppControllers.controller('QualityController', ['$location', function ($location) {		
	loadQuality();
}]);


function loadQuality(){

//	displayLoading('qualityTestOutput');
//	displayLoading('mtqi');
	var callURL = '/frengly/static/quality.json';
	$.ajax({
		url:  callURL,		
		contentType: 'application/json;charset=utf-8',		
	    type: 'GET',
	    dataType: 'json',
		
	    data: {
			text : $('#qualityInput').val(),
			qualityLang: $('#qualityLang').html(),
		},
		
		success: function(data){
//			hideLoading('qualityTestOutput');
//			hideLoading('mtqi');
			
			//$('#qualityTestOutput').html(data.html);
			$('#qualityOutput').empty();
			$('#mtqi').html(data.mtqi);
			
			try{	
			//------------------- Display translation as list --------------------------
			$.each(data.list, function(i, row) {
				
				var srcLang = 'en';
				var delimiter = '\n';
				var uniqueId = row.uniqueId;				
				var src = row.srcWord;
				src = src.replace(new RegExp(delimiter, 'g'),'<br>');
			    var dest = row.destWord;
			    dest = dest.replace(new RegExp(delimiter, 'g'),'<br>');
			        
			    var enId = row.srcId;
			    var pid = row.destId;
			   
			    if (srcLang=='en') {
			    	enId = row.srcId;
			    	pid = row.destId;
			    } else {
			    	enId = row.destId;
			    	pid = row.srcId;
			    }
			    
			    var border = '';
			    
			    /*
			    if (row.isDelimiter=='1'){
			    	border = '';
			    } else if (pid.indexOf('stamp')>-1){
			    	border = 'border-bottom: 2px solid red;'
			    } else {
			    	border = 'border-bottom: 1px solid black;';
			    }*/
			    
			    var styleCursor = 'cursor: pointer;';
			    //if (row.score==0.0) styleCursor = '';
			    
				$('#qualityOutput').append('<span style="'+styleCursor+'" title="'+row.score+'" style="'+border+'">'+dest+'</span>');
				$('#qualityOutput').children().last().hover(
						  function () {
						    $(this).addClass("hov");
						  },
						  function () {
						    $(this).removeClass("hov");
						  }
				);
				
				
				
			    //$('#qualityOutput').append('adasdas');
			});
			
			}catch(ex){alert(ex);}
			//------------------- Display translation as list --------------------------
			
		}
		
	});

}