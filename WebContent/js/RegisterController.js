demoApp.controller('RegisterController', ['$http', '$scope', '$location', 'registration', function ($http, $scope, $location, registration) {	
	
	$scope.registerUser = function() {
		//register($location);
		console.log('inside register controller');

		//DATA
        var data = {
        		email: $scope.user.email, 
        		password: $scope.user.password, 
        		rest: '1'
        }        
        //SUCCESS
        var success = function(data){
        	  $location.path('/translate');
	          toastr.info('You have registered successfully');	          
	    }	    
        //ERROR
	    var error = function(data){    	
	    	 toastr.error('Registration failure: '+data);    	  
	    }
      
	    
	    //LOGIN
	    registration.register(data).success(success).error(error);
		
		
		//it cannot go to jquery success function as angular 
		//expects location to be changed synchronously
		
		
	}
}]);


/*function register(location){
	//url: rootURL+'controller?action=register&rest=1',
	var callURL = '/frengly/data/register';
	$.ajax({
		url:  callURL,
		type: 'POST',
		dataType: 'json',		
        contentType: 'application/json;charset=utf-8',
        data: {
			rest: 1,
			email:
			password:
			
		}
        //server will return 400 bad requets, if one of those props will not match props from a server object!!!!
        //so better first put only one simple property
              
	  	success: function(data){
	  		
	  	},	  	
		error: function (errormessage) {
			alert('error');
        }
	});	
	
}*/