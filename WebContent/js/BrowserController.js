demoApp.controller('BrowserController', ['$http', '$scope', '$rootScope', '$location', '$cookieStore', 'topicManager', function ($http,$scope, $rootScope, $location, $cookieStore, topicManager) {	
	

	$scope.deleteDictEntry = function(id){		
		//english is added as dummy value only to satisfy API
		$http['delete']('/frengly/data/deleteDictEntry?pid='+id+'&srcLang='+$scope.selectedLang+'&destLang=English').success(function(data, status, headers, config) {$scope.loadDict();}).error(function(data, status, headers, config) {});		
	}	
	
	$scope.deleteAllFound = function(id){		
		$http['delete']('/frengly/data/deleteDictSearch?lang='+$scope.selectedLang).success(function(data, status, headers, config) {$scope.loadDict();}).error(function(data, status, headers, config) {});		
	}	
	
	$scope.addTopic = function() {        
		
		var data = {
			topicName: $scope.topicName
		};
		
		var success = function(data){			
			//loadTopics($scope);
			//$scope.topicGrid.refresh();
			$scope.loadTopicNew();
		}
		
		var error = function(data){ 
		}
		
		topicManager.create(data).success(success).error(error);
	}
	
	$scope.loadDict = function() {		
		$http.post('/frengly/data/dict',{text: $scope.text, lang: $scope.selectedLang, status: $scope.selectedStatus}).success(function(data, status, headers, config) {$scope.rows = data;}).error(function(data, status, headers, config) {});
	}
	$scope.loadStatuses = function() {		
		$http.get('/frengly/data/dictStatuses?lang='+$scope.selectedLang).success(function(data, status, headers, config) {$scope.statuses = data;}).error(function(data, status, headers, config) {});
	}
	
	$scope.selectedLang=$rootScope.langs["fr"];	
	$scope.loadDict();
	$scope.loadStatuses();
	

}]);