demoApp.factory('contribution', ['$http',function ($http) {
  return {
      addPair: function (data) {
          return $http.post('/frengly/data/addPairREST', data);
      }
  };
}]);

demoApp.factory('badges', ['$http', function ($http) {
	  return {
	      load: function (data) {
	          return $http.get('/frengly/data/badges', data);
	      }
	  };
	}]);



demoApp.factory('authentication', ['$http', '$cookieStore', '$rootScope', '$location', function ($http, $cookieStore, $rootScope, $location) {

  return {
      login: function (credentials) {
          return $http.post('/frengly/data/login', credentials);
      },
      
      logout: function () {
    	  $cookieStore.put('rememberme', null);
    	  $cookieStore.put('roles', null);
    	  $rootScope.username = null;    	  
          return $http.get('/frengly/data/logout');
      },
  
  	  deleteAccount: function () {
          return $http['delete']('/frengly/data/account');
      }
  };
}]);

demoApp.factory('registration', ['$http',function ($http) {

	  return {
	      register: function (data) {
	          return $http.post('/frengly/data/register', data);
	      }
	  };
	}]);

demoApp.factory('translation', ['$http',function ($http) {
	  return {
	      translate: function (data) {
	          return $http.post('/frengly/data/translate/', data);
	      }
	  };
	}]);

demoApp.factory('postManager', ['$http',function ($http) {

	  return {
	      create: function (data) {
	          return $http.post('/frengly/data/post', data);
	      }
	  };
}]);

demoApp.factory('topicManager', ['$http',function ($http) {

	  return {
	      create: function (data) {
	          return $http.post('/frengly/data/topic', data);
	      }
	  };
}]);



demoApp.factory('RoleService', ['$http',function ($http) {
	var adminRoles = ['admin', 'editor'];
	var otherRoles = ['user'];

	return {
		validateRoleAdmin: function (currentUser) {
			return currentUser ? _.contains(adminRoles, currentUser.role) : false;
		},

		validateRoleOther: function (currentUser) {
			return currentUser ? _.contains(otherRoles, currentUser.role) : false;
		}
	};
}]);


demoApp.factory('UserDBService', ['$http',function ($http) {
	
	var allRoles = ['admin', 'translator','user','anonymous'];
	var dbUsers = [{id: 100, name: 'info@frengly.com', email: 'info@frengly.com', password: '123', roles: ['admin']},
			        {id: 101, name: 'john', email: 'yy@gg.pl', password: 'xyz123', roles: ['translator']}];
	
	return {

		getUsers: function () {			
			return dbUsers;
		},

		authenticate: function (user) {
			var authenticated = null;
				
/*			 angular.forEach(dbUsers, function (obj) {
					  if (obj.email==user.email && obj.password==user.password){
						  authenticated = obj;					  
					  }
			 	});*/

			var callURL = '/frengly/data/login';
					$.ajax({
						url: callURL,
						data: {
							username: user.email,
							password: user.password
						},	        
						method: 'post',
						success: function(data){
							if (data.ticket!=null){																
								username = data.username;
								authenticated = {id: data.userId, name: username, email: username, password: 'dummy', roles: ['translator']};
								
							}
						}
					});
			
			
			
			return authenticated;
		}
	};
}]);

//only for DEV
/*demoApp.factory('httpRequestInterceptor', function () {
	  return {
	    request: function (config) {		  
		  if (location.host.indexOf('localhost')>-1 && config.url.indexOf('frengly.com')<0 && (config.url.indexOf('static')>-1 || config.url.indexOf('data')>-1)){
			  config.url = "http://frengly.com" + config.url.substring(8); 			  			  
		  }
	      return config;
	    }
	  };
});
	 
demoApp.config(['$httpProvider',function ($httpProvider) {
	  $httpProvider.interceptors.push('httpRequestInterceptor');
}]);*/